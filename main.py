from flask import Flask, app
from flask import request
from mysql.connector.fabric import connect
from database.config import *

app = Flask(__name__)

@app.route('/api/v1/entitlement/user/play', methods=['POST'])
def user_play():
    content_id = request.json.get('content_id', '0')
    user_id = request.json.get('user_id', '0') 

    query = """SELECT IF(COUNT(T2.ContentBundle_id_content_bundle   ) > 0, "1", "0")
    FROM (SELECT ContentBundle_id_content_bundle FROM Package_Having_ContentBundle phcb WHERE phcb.Package_id_package IN 
    (SELECT udhp.Package_id_package FROM UserDetail_Having_Package udhp WHERE udhp.UserDetail_UDKey = %(user_id)s )) 
    T1 INNER JOIN (SELECT ContentBundle_id_content_bundle 
    FROM ContentBundle_Having_Content cbhc WHERE cbhc.Content_id_content = %(content_id)s ) 
    T2 ON T1.ContentBundle_id_content_bundle = T2.ContentBundle_id_content_bundle"""
    
    cursor = connect.cursor()
    cursor.execute(query, {'content_id':content_id, 'user_id':user_id})
    results = cursor.fetchone()
    return {
        "can_play" : results[0] 
    }

if __name__ == '__main__':
    app.run(debug=True)
